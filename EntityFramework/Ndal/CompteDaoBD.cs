﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.NBanque;
using EntityFramework.NEf;

namespace EntityFramework.Ndal
{
    class CompteDaoBD : ICompteDAO
    {
        private ContextEf context;

        public CompteDaoBD()
        {
            this.context = new ContextEf();
        }

        public Compte GetCompte(string numero)
        {
            return context.CompteEntities
                .Where(cp => cp.Numero.Equals(numero))
                .FirstOrDefault();
        }

        public (string, float) GetNumeroAndSolde(string numero)
        {
            var res = context.CompteEntities
                        .Where(cpt => cpt.Numero.Equals(numero))
                        .Select(cpt => new { cpt.Numero, cpt.Solde })
                        .FirstOrDefault();
            return (res.Numero, res.Solde);
        }

        public Tuple<string, float> GetNumeroAndSoldeTuple(string numero)
        {
            Tuple<string, float> res = context.CompteEntities
                        .Where(cpt => cpt.Numero.Equals(numero))
                        .Select(cpt => new { cpt.Numero, cpt.Solde })
                        .AsEnumerable()
                        .Select(cpt => new Tuple<string, float>(cpt.Numero, cpt.Solde))
                        .FirstOrDefault();
            return (res);
        }

        public string GetInformationCompte()
        {
            StringBuilder resultatInfo = new StringBuilder();

            resultatInfo.AppendLine("Information sur les comptes");
            resultatInfo.AppendLine("Nombre de compte : " + context.CompteEntities.Count());

            resultatInfo.AppendLine("Total des soldes : " + context.CompteEntities.Sum(cpt => cpt.Solde));

            resultatInfo.AppendLine("Moyenne des différents soldes : " + context.CompteEntities.Average(cpt => cpt.Solde));


            resultatInfo.AppendLine("Solde minimum des comptes : " + context.CompteEntities.Min(cpt => cpt.Solde));
            resultatInfo.AppendLine("Solde maximum des comptes : " + context.CompteEntities.Max(cpt => cpt.Solde));

            return resultatInfo.ToString();
        }

        public bool AjouterCompte(Compte compte)
        {
            context.CompteEntities.Add(compte);
            int nbrLine = context.SaveChanges();
            return nbrLine != 0 ? true : false;
        }

        public bool SupprimerCompte(string numero)
        {
            Compte compteToDelete = GetCompte(numero);
            context.CompteEntities.Remove(compteToDelete);
            int nbrLineAffected = context.SaveChanges();
            return nbrLineAffected != 0 ? true : false;

        }

        public void ModifierCompte(Compte compte, float montant)
        {
            compte.Solde += montant;
            context.SaveChanges();
        }

        public Compte GetCompteDirectSQL(string numero)
        {
            string [] parametres = { numero };
            string query = "SELECT IdCompte as CompteId," +
                "C_Numero as Numero," +
                "C_Solde as Solde," +
                "C_DateCreation as DateCreation " +
                "FROM T_Comptes " +
                "WHERE C_Numero=@p0";
            return context.Database.SqlQuery<Compte>(query, parametres).FirstOrDefault();
        }
    }
}
