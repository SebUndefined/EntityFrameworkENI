﻿using EntityFramework.NBanque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Ndal
{
    public interface ICompteDAO
    {

        Compte GetCompte(string numero);

        Compte GetCompteDirectSQL(string numero);

        (string, float) GetNumeroAndSolde(string numero);


        Tuple<string, float> GetNumeroAndSoldeTuple(string numero);


        string GetInformationCompte();

        bool AjouterCompte(Compte compte);

        bool SupprimerCompte(String numero);

        void ModifierCompte(Compte compte, float montant);
    }
}
