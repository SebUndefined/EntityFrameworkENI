﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.NBanque;
using EntityFramework.NUtil;

namespace EntityFramework.Ndal
{
    class CompteDAO : ICompteDAO
    {
        public List<Compte> ListeComptes { get; }

        public CompteDAO()
        {
            ListeComptes = Util.GetComptes();
        }

        public CompteDAO(List<Compte> listeComptes)
        {
            ListeComptes = listeComptes;
        }

        public Compte GetCompte(string numéro)
        {
            return ListeComptes.Where(compte => compte.Numero.Equals(numéro))
                    .FirstOrDefault();
        }

        public (string, float) GetNumeroAndSolde(string numero)
        {
            var res = ListeComptes
                        .Where(cpt => cpt.Numero.Equals(numero))
                        .Select(cpt => new { cpt.Numero, cpt.Solde })
                        .FirstOrDefault();
            return (res.Numero, res.Solde);
        }

        public Tuple<string, float> GetNumeroAndSoldeTuple(string numero)
        {
            Tuple<string, float> res = ListeComptes
                        .Where(cpt => cpt.Numero.Equals(numero))
                        .Select(cpt => new Tuple<string, float>( cpt.Numero, cpt.Solde ))
                        .FirstOrDefault();
            return (res);
        }

        public string GetInformationCompte()
        {
            StringBuilder resultatInfo = new StringBuilder();

            resultatInfo.AppendLine("Information sur les comptes");
            resultatInfo.AppendLine("Nombre de compte : " + ListeComptes.Count());

            resultatInfo.AppendLine("Total des soldes : " + ListeComptes.Sum(cpt => cpt.Solde));

            resultatInfo.AppendLine("Moyenne des différents soldes : " + ListeComptes.Average(cpt => cpt.Solde));


            resultatInfo.AppendLine("Solde minimum des comptes : " + ListeComptes.Min(cpt => cpt.Solde));
            resultatInfo.AppendLine("Solde maximum des comptes : " + ListeComptes.Max(cpt => cpt.Solde));

            return resultatInfo.ToString();

        }

        public bool AjouterCompte(Compte compte)
        {
            throw new NotImplementedException();
        }

        public bool SupprimerCompte(string numero)
        {
            throw new NotImplementedException();
        }

        public void ModifierCompte(Compte compte, float montant)
        {
            throw new NotImplementedException();
        }

        public Compte GetCompteDirectSQL(string numero)
        {
            throw new NotImplementedException();
        }
    }
}
