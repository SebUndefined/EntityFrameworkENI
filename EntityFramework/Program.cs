﻿using EntityFramework.NBanque;
using EntityFramework.Ndal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            NUtil.Util.InitTable();

            ICompteDAO compteDAO = new CompteDaoBD();

            Compte compte = compteDAO.GetCompte("101");

            Console.WriteLine(compte.ToString());

            (string numero, float solde) = compteDAO.GetNumeroAndSolde("101");
            Console.WriteLine($"Numéro: {numero} Solde: {solde}");

            Tuple<string, float> resTuple = compteDAO.GetNumeroAndSoldeTuple("101");
            Console.WriteLine($"Numéro: {resTuple.Item1} Solde: {resTuple.Item2}");

            Console.WriteLine(compteDAO.GetInformationCompte());


            Console.WriteLine("Ajout d'un compte");
            Compte compte2 = new Compte("129", 129, DateTime.Now);
            compteDAO.AjouterCompte(compte2);

            Console.WriteLine("Modifier Compte");
            compteDAO.ModifierCompte(compte2, 200);
            Compte compte3 = compteDAO.GetCompte("129");
            Console.WriteLine(compte3);


            Console.WriteLine("Suppression de compte");
            bool res = compteDAO.SupprimerCompte("129");
            if(res == true)
            {
                Console.WriteLine("La suppression est effective !!");
            }
            else
            {
                Console.WriteLine("Compte non supprimé");
            }
            Console.WriteLine("GetCompte en SQL");
            Compte compte4 = compteDAO.GetCompteDirectSQL("101");
            Console.WriteLine(compte4.ToString());
            Console.ReadLine();


        }
    }
}
