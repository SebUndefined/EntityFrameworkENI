﻿using EntityFramework.NBanque;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.NEf
{
    public class ContextEf : DbContext
    {
        public ContextEf() : base("C_BdVideo")
        {
        }

        public DbSet<Compte> CompteEntities { get; set; }

        //Personnaliser mécanisme de création de la base (string => varchar etc...)
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Compte>().ToTable("T_Comptes", "dbo");
            modelBuilder.Entity<Compte>().Property(c => c.Numero).HasColumnName("C_Numero");
            modelBuilder.Entity<Compte>().Property(c => c.Numero).HasMaxLength(20);
        }
    }
}
