﻿using EntityFramework.NBanque;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.NEf
{
    class InitCompte : DropCreateDatabaseIfModelChanges<ContextEf>
    {

        protected override void Seed(ContextEf context)
        {
            base.Seed(context);
            context.CompteEntities.Add(new Compte("101", 100, new DateTime(2016, 10, 10)));
            context.CompteEntities.Add(new Compte("102", 200, new DateTime(2017, 2, 12)));
            context.CompteEntities.Add(new Compte("103", 348F, new DateTime(2018, 3, 22)));
            context.CompteEntities.Add(new Compte("104", 234.65F, new DateTime(2015, 11, 4)));
            context.CompteEntities.Add(new Compte("105", 10234.98F, new DateTime(2014, 12, 6)));
            context.CompteEntities.Add(new Compte("106", 2034449.45F, new DateTime(2018, 1, 4)));
            context.CompteEntities.Add(new Compte("107", 3090000.32F, new DateTime(2017, 7, 3)));

            context.SaveChanges();
        }
    }
}
