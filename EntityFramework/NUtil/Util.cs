﻿using EntityFramework.NBanque;
using EntityFramework.NEf;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.NUtil
{
    static class Util
    {
        public static List<Compte> GetComptes()
        {
            List<Compte> listComptes = new List<Compte>() {
                new Compte("101", 300, new DateTime(2016, 10, 10)),
                new Compte("102", 200, new DateTime(2017, 2, 12)),
                new Compte("103", 348F, new DateTime(2018, 3, 22)),
                new Compte("104", 234.65F, new DateTime(2015, 11, 4)),
                new Compte("105", 10234.98F, new DateTime(2014, 12, 6)),
                new Compte("106", 2034449.45F, new DateTime(2018, 1, 4)),
                new Compte("107", 3090000.32F, new DateTime(2017, 7, 3)),
            };
            return listComptes;
            
        }

        public static void CreateTable()
        {
            var initializer = new DropCreateDatabaseIfModelChanges<ContextEf>();
            initializer.InitializeDatabase(new ContextEf());
        }

        public static void InitTable()
        {
            var initializer = new InitCompte();
            initializer.InitializeDatabase(new ContextEf());
        }
    }
}
