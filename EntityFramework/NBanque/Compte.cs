﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.NBanque
{
    [Table("T_Comptes")]
    public class Compte
    {
        #region Properties
        [Key]
        [Column("IdCompte")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CompteId { get; set; }

        [Column("C_Numero")]
        [Required]
        [MaxLength(20)]
        public string Numero { get; set; }

        [Column("C_Solde")]
        [DataType("Real")]
        public float Solde { get; set; }

        [Column("C_DateCreation")]
        [Required]
        public DateTime DateCreation { get; set; }

        #endregion

        #region Constructor
        public Compte(string numero, float solde, DateTime dateCreation)
        {
            Numero = numero;
            this.Solde = solde;
            DateCreation = dateCreation;
        }

        public Compte(string numero)
        {
            Numero = numero;
            DateCreation = DateTime.Now;
            Solde = 0;
        }
        
        public Compte() : this("Undefined") { }
        #endregion

        #region methodes
        public override string ToString()
        {
            return "Compte numéro: " + this.Numero + " Date de création:" + this.DateCreation + " Solde: " + this.Solde;
        }
        #endregion
    }
}
