namespace DemoEFFromBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_Comptes
    {
        [Key]
        public long IdCompte { get; set; }

        [Required]
        [StringLength(20)]
        public string C_Numero { get; set; }

        public float C_Solde { get; set; }

        public DateTime C_DateCreation { get; set; }
    }
}
